#rails template box

## Info 

Ruby on Rails development template vagrant box.

## Installed

* Ruby:2.4.0
* MySQL:5.7.1
* Redis:3.2.8
* nginx:1.10.2


### MySQL

| User | Pass |
|---|---|
|root|Vagrant@72|
|vagrant|Vagrant@72|

Database:vagrant

### Redis

Default setting.